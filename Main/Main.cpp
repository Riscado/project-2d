#include <SFML\Graphics.hpp>

sf::RenderWindow* window;

const int windowWidth = 1400;
const int windowHeight = 800;
const std::string windowName = "SFML App";

int fps = 0;
double deltaTime = 0;

int main() {
	window = new sf::RenderWindow(sf::VideoMode(windowWidth, windowHeight), windowName);

	sf::Clock deltaClock;
	sf::Clock fpsClock;
	int fpsCount = 0;

	while (window->isOpen()) {
		sf::Event event;
		while (window->pollEvent(event)) {
			switch (event.type) {
			case sf::Event::Closed:
				window->close();
				break;
			}
		}

		window->clear();

		window->display();

		fpsCount++;
		if (fpsClock.getElapsedTime().asMilliseconds() >= 1000) {
			fps = fpsCount;
			fpsCount = 0;
			window->setTitle(windowName + ": " + std::to_string(fps));
			fpsClock.restart();
		}

		deltaTime = deltaClock.restart().asSeconds();
	}

	delete window;
}